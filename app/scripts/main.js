$(document).ready(function () {

  $('.phone').inputmask('+7(999)999-99-99');

  $('.k-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.k-header__row').slideToggle('fast');
    $('.k-nav').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.k-modal').slideToggle('fast');
  });

  $('.k-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.k-modal').slideToggle('fast');
  });

  $('#slider-people').rangeslider({
    polyfill: false,
    onInit: function () {},
    onSlide: function (position, value) {
      $('#input-people').val(value);
    },
  });

  $('#input-people').on('change', function () {
    $('#slider-people').val($(this).val()).change();
  });

  $('#modal-slider-people').rangeslider({
    polyfill: false,
    onInit: function () {},
    onSlide: function (position, value) {
      $('#modal-input-people').val(value);
    },
  });

  $('#modal-input-people').on('change', function () {
    $('#modal-slider-people').val($(this).val()).change();
  });

  $('.k-home-big').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false
      }
    }]
  });

  $('.k-videos').slick({
    dots: false,
    arrows: true,
    infinite: true,
    prevArrow: $('.k-videos__prev'),
    nextArrow: $('.k-videos__next'),
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false
      }
    }]
  });

  $('.k-photos').slick({
    dots: false,
    arrows: true,
    infinite: true,
    prevArrow: $('.k-photos__prev'),
    nextArrow: $('.k-photos__next'),
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    slidesPerRow: 2,
    rows: 2,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        centerMode: false,
        dots: false
      }
    }]
  });

  $('.k-about-slick').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false
      }
    }]
  });

  $('.k-partners__cards').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }]
  });

  var $sticky = $('.sticky');
  var $stickyrStopper = $('.sticky-stopper');
  if (!!$sticky.offset()) { // make sure ".sticky" element exists

    var generalSidebarHeight = $sticky.innerHeight();
    var stickyTop = $sticky.offset().top;
    var stickOffset = 0;
    var stickyStopperPosition = $stickyrStopper.offset().top - 200;
    var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
    var diff = stopPoint + stickOffset;

    $(window).scroll(function () { // scroll event
      var windowTop = $(window).scrollTop(); // returns number

      if (stopPoint < windowTop) {
        $sticky.css({
          position: 'absolute',
          top: diff
        });
      } else if (stickyTop < windowTop + stickOffset) {
        $sticky.css({
          position: 'fixed',
          top: stickOffset
        });
      } else {
        $sticky.css({
          position: 'absolute',
          top: 'initial'
        });
      }
    });

  }
});
